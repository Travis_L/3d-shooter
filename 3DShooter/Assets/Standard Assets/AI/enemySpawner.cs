﻿using UnityEngine;
using System.Collections;

public class enemySpawner : MonoBehaviour {
    public Rigidbody SnowMan;
    public static Rigidbody SnowManClone;
    public static int EnemyCount = 0; 
	// Use this for initialization
	void Start () 
    {
        InvokeRepeating("Spawn", 1,2); 
	}
	
	// Update is called once per frame
    public static void Kill()
    {
        DestroyObject(SnowManClone.gameObject);
    }

    void Spawn()
    {
        Spawner();
        EnemyCount++;
    }

    void Spawner()
    {
       SnowManClone = (Rigidbody)Instantiate(SnowMan, transform.position, transform.rotation);
    }
}
