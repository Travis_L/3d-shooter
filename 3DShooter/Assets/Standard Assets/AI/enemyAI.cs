﻿using UnityEngine;
using System.Collections;

public class enemyAI : MonoBehaviour {
    public Transform target;
    public int movementSpeed;
    public int rotationSpeed;
    public Transform myTransform;


	// Use this for initialization
	void Start () 
    {
        myTransform = transform;
        target = GameObject.FindWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () 
    {
        //Rotate to look at the player
        myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed * Time.deltaTime);
        //Follow the Player
        myTransform.position += myTransform.forward * movementSpeed * Time.deltaTime; 
	}
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            print("I have being hit");
            enemySpawner.EnemyCount -= 1;
            GameObject.Destroy(gameObject);
        }
    }
}
