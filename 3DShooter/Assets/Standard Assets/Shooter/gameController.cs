﻿using UnityEngine;
using System.Collections;

public class gameController : MonoBehaviour {
    public bool lockCursor;
    public bool showCursor; 
	// Use this for initialization
	void Start () {
        lockCursor = true;
        showCursor = false;
       
	}
	
	// Update is called once per frame
	void Update () {
        Screen.lockCursor = lockCursor;
        Screen.showCursor = showCursor; 
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            lockCursor = false; 
            showCursor = true;
        }
	}
}
