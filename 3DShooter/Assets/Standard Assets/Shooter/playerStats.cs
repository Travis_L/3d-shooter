﻿using UnityEngine;
using System.Collections;

public class playerStats : MonoBehaviour {

    public int playerHealth = 3;

    void OnControllerColliderHit(ControllerColliderHit collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            playerHealth -= 1;
        }
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (playerHealth < 1)
        {
            Application.LoadLevel(0); 
        }
        print(playerHealth); 
	}
}
