﻿using UnityEngine;
using System.Collections;

public class shootGun : MonoBehaviour {
    public Rigidbody bullet;
    public int bulletSpeed; 
   
	// Use this for initialization
	void Start () {
        
	}

    void Update () 
    {

        if (Input.GetMouseButtonDown(0))
        {
            Shoot(); 
        }

	}
    void Shoot()
    {
        Rigidbody bulletClone = (Rigidbody)Instantiate(bullet, transform.position, transform.rotation);
        bulletClone.velocity = transform.forward * bulletSpeed;
        Destroy(bulletClone.gameObject, 5);
    }
}
